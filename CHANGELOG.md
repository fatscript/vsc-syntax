# Changelog

Changes to the this VSC extension will be documented in this file.

## Version 3.4.0

- add support for procedure syntax <>

## Version 3.0.0

- add support for tap operator <<

## Version 2.4.0

- add support for $break command special highlight

## Version 2.3.0

- add support for switch operator >>

## Version 2.2.0

- add support for 'infinity' keyword

## Version 2.1.0

- add support for HugeInt literals 0xf1e2d3...

## Version 2.0.0

- add support for 'self' and 'root' keywords

## Version 1.2.1

- add support for \e as escaped code for escape

## Version 1.2.0

- Add half-open range operator support

## Version 1.1.3

- Fix tmLanguage naming

## Version 1.1.2

- Revert logo weird effect

## Version 1.1.1

- Fix for type name matching
- Add glow effect to logo

## Version 1.1.0

- Accept accented unicode characters on entry and type names
- Accept underscores within entry and type names

## Version 1.0.1

- Update logo to match new branding

## Version 1.0.0

- Initial release
