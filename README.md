# FatScript Syntax Highlighting (VSC Extension)

This project provides syntax highlighting for the FatScript programming language in Visual Studio Code. For more information about FatScript language specification and syntax, please visit:

- [fatscript.org](https://fatscript.org) (official docs)

## Installation

1. Install Visual Studio Code if you haven't already.
2. Open the extensions panel.
3. Search for "FatScript Syntax".
4. Click the "Install" button for the extension.

Alternatively, you can install the extension from source:

1. Clone or download the repository to your local machine.
2. Install the [Visual Studio Code Extension SDK](https://code.visualstudio.com/docs/extensions/example-hello-world#_step-1-set-up-your-development-environment).
3. Run `vsce package` to create the VSIX package for the extension.
4. In Visual Studio Code, click on the "..." icon in the extensions panel and select "Install from VSIX".

## Usage

Once the FatScript syntax highlighting extension is installed, it will automatically highlight FatScript code files (.fat) with the appropriate syntax highlighting.

## Contributing

If you find any issues with this syntax highlighting extension or would like to contribute to the project, please [create an issue](https://gitlab.com/fatscript/vsc-syntax/issues) or merge request on the project's [GitLab repository](https://gitlab.com/fatscript/vsc-syntax).

### Donations

Did you find this VSC extension useful and would like to say thanks?

[Buy me a coffee](https://www.buymeacoffee.com/aprates)

## License

[GPLv3](LICENSE) © 2023-2024 Antonio Prates
